package com.example.backend.dao;

import com.example.backend.models.TarjetaVideo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TarjetaVideoDaoAPI extends JpaRepository<TarjetaVideo,Long> {
}
